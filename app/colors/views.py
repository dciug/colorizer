#!/usr/bin/env python

from django.http import HttpResponseRedirect
from django.shortcuts import render

import matplotlib.pyplot as plt

from .forms import DocumentForm, Document

import os
from .danielnet_floyd_classify import colorize, DanielNet
#from .danielnet_floyd_regression import colorize, DanielNet

class Singleton(object):
    class __Model:
        def __init__(self):
            self.val = DanielNet(include_top=False, weights='imagenet', input_shape=(224, 224, 3))
            self.val.load_weights('./models/model_floyd_classify.h5')
    instance = None

    def __new__(cls):
        if not Singleton.instance:
            Singleton.instance = Singleton.__Model()

        return Singleton.instance


def compare_images(request):
    docs = list(Document.objects.all())
    original = './media/' + docs[-1].file.name

    model = Singleton()

    if not os.path.isdir('./media/images/colorized'):
        os.makedirs('./media/images/colorized')

    modified = './media/images/colorized/' + original.split('/')[-1]

    colorized_image = colorize(model.val, original)
    plt.imsave(modified, colorized_image)

    return render(request, 'colors/index.html', { 'original': original[1:],
                                                  'modified': modified[1:], })

def upload_file(request):
    if request.method == 'POST':
        form = DocumentForm(request.POST, request.FILES)
        if form.is_valid():
            new_file = Document(file=request.FILES['file'])
            new_file.save()

            return HttpResponseRedirect('compare/')
    else:
        form = DocumentForm()

    return render(request, 'colors/upload.html')
