from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.upload_file, name='upload_file'),
    url(r'^compare/', views.compare_images, name='compare_images'),
]
