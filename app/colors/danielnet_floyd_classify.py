#-*- coding: utf-8 -*-
'''DanielNet model for Keras.'''

# Standard imports
import warnings

# Keras imports
from keras import layers
from keras import optimizers
from keras.layers import Activation
from keras.layers import BatchNormalization
from keras.layers import Conv2D
from keras.layers import Dense
from keras.layers import Dropout
from keras.layers import Flatten
from keras.layers import Reshape
from keras.layers import Input
from keras.layers import MaxPooling2D
from keras.layers import GlobalMaxPooling2D
from keras.layers import AveragePooling2D
from keras.layers import GlobalAveragePooling2D
from keras.layers import UpSampling2D

from keras.applications.imagenet_utils import preprocess_input
from keras.applications.imagenet_utils import decode_predictions
from keras.applications.imagenet_utils import _obtain_input_shape

from keras.engine.topology import get_source_inputs

from keras.models import Model

from keras.preprocessing import image
from keras.utils import layer_utils
from keras.utils import to_categorical
#from keras.utils import plot_model
from keras.utils.data_utils import get_file

import keras.backend as K
from keras.callbacks import ModelCheckpoint, History

# External imports
import cv2
import numpy as np
from PIL import ImageFile
from .utils import *
ImageFile.LOAD_TRUNCATED_IMAGES = True

import skimage
import skimage.io
from scipy.ndimage import zoom
from skimage.transform import resize
import skimage.color as color
import scipy.ndimage.interpolation as sni


# Gloal parameters
BATCH_SIZE = 8
EPOCHS = 10000
NR_BINS = 64
WEIGHTS_PATH = 'https://github.com/fchollet/deep-learning-models/releases/download/v0.1/vgg16_weights_tf_dim_ordering_tf_kernels.h5'
WEIGHTS_PATH_NO_TOP = 'https://github.com/fchollet/deep-learning-models/releases/download/v0.1/vgg16_weights_tf_dim_ordering_tf_kernels_notop.h5'
TRAIN_PATH = '/input'


def _get_files(img_dir):
    files = list_files(img_dir)
    return list(map(lambda x: os.path.join(img_dir, x), files))


# Helper functions
def load_image(filename, color=True):
    """
    Load an image converting from grayscale or alpha as needed.
    Parameters
    ----------
    filename : string
    color : boolean
        flag for color format. True (default) loads as RGB while False
        loads as intensity (if image is already grayscale).
    Returns
    -------
    image : an image with type np.float32 in range [0, 1]
        of size (H x W x 3) in RGB or
        of size (H x W x 1) in grayscale.
    """
    img = skimage.img_as_float(skimage.io.imread(filename, as_grey=not color)).astype(np.float32)
    if img.ndim == 2:
        img = img[:, :, np.newaxis]
        if color:
            img = np.tile(img, (1, 1, 3))
    elif img.shape[2] == 4:
        img = img[:, :, :3]

    return img


def resize_image(im, new_dims, interp_order=1):
    """
    Resize an image array with interpolation.
    Parameters
    ----------
    im : (H x W x K) ndarray
    new_dims : (height, width) tuple of new dimensions.
    interp_order : interpolation order, default is linear.
    Returns
    -------
    im : resized ndarray with shape (new_dims[0], new_dims[1], K)
    """
    if im.shape[-1] == 1 or im.shape[-1] == 3:
        im_min, im_max = im.min(), im.max()
        if im_max > im_min:
            # skimage is fast but only understands {1,3} channel images
            # in [0, 1].
            im_std = (im - im_min) / (im_max - im_min)
            resized_std = resize(im_std, new_dims, order=interp_order, mode='reflect')
            resized_im = resized_std * (im_max - im_min) + im_min
        else:
            # the image is a constant -- avoid divide by 0
            ret = np.empty((new_dims[0], new_dims[1], im.shape[-1]),
                           dtype=np.float32)
            ret.fill(im_min)
            return ret
    else:
        # ndimage interpolates anything but more slowly.
        scale = tuple(np.array(new_dims, dtype=float) / np.array(im.shape[:2]))
        resized_im = zoom(im, scale + (1,), order=interp_order)

    return resized_im.astype(np.float32)


def prepare_image(path, target_size=(224, 224)):
    img_rgb = load_image(path) # load image
    img_lab = color.rgb2lab(img_rgb) # convert image to lab color space
    img_l = img_lab[:, :, 0] # pull out L channel
    (H_orig, W_orig) = img_rgb.shape[:2] # original image size

    # Save model dimensions
    H_in, W_in = target_size
    H_out, W_out = target_size

    # resize image to network input size
    img_rs = resize_image(img_rgb, (H_in, W_in)) # resize image to network input size
    img_lab_rs = color.rgb2lab(img_rs)
    img_l_rs = img_lab_rs[:, :, 0]
    img_a_rs = img_lab_rs[:, :, 1]
    img_b_rs = img_lab_rs[:, :, 2]

    img_l_rs /= 100 # Normalize L between 0 and +1

    # Normalize a*b* between 0 and +1
    img_a_rs = np.clip(((img_a_rs + 86.185) * NR_BINS / 184.439), 0, NR_BINS-1)
    img_b_rs = np.clip(((img_b_rs + 107.863) * NR_BINS / 202.345), 0, NR_BINS-1)

    img_a_rs = img_a_rs.flatten()
    img_b_rs = img_b_rs.flatten()

    img_a_rs = to_categorical(img_a_rs, num_classes=NR_BINS)
    img_b_rs = to_categorical(img_b_rs, num_classes=NR_BINS)

    img_l_rs_merged = np.array([img_l_rs, img_l_rs, img_l_rs]).transpose(1, 2, 0)

    return img_l_rs_merged, img_a_rs, img_b_rs


def colorize(model, path, target_size=(224, 224), NR_BINS=64):
    img_rgb = load_image(path)
    img_lab = color.rgb2lab(img_rgb) # convert image to lab color space
    img_l = img_lab[:, :, 0] # pull out L channel
    img_a = img_lab[:, :, 1]
    img_b = img_lab[:, :, 2]
    (H_orig, W_orig) = img_rgb.shape[:2] # original image size

    H_in, W_in = target_size
    H_out, W_out = target_size

    # resize image to network input size
    img_rs = resize_image(img_rgb, (H_in, W_in)) # resize image to network input size
    img_lab_rs = color.rgb2lab(img_rs)
    img_l_rs = img_lab_rs[:, :, 0]

    img_a_rs = img_lab_rs[:, :, 1]
    img_b_rs = img_lab_rs[:, :, 2]
    img_l_rs /= 100
    img_l_rs = np.expand_dims(img_l_rs, axis=2)
    img_l_rs = cv2.merge([img_l_rs, img_l_rs, img_l_rs]) # concat 3 times, to match the input to VGG
    img_l_rs = np.expand_dims(img_l_rs, axis=0)

    img_ab_rs = img_lab_rs[:, :, 1:3]

    ab = model.predict(img_l_rs)

    a = np.squeeze(ab[0], axis=0)
    b = np.squeeze(ab[1], axis=0)
    a_tensor = a.reshape((224, 224, NR_BINS))
    b_tensor = b.reshape((224, 224, NR_BINS))
    a_image = np.zeros((224, 224))
    b_image = np.zeros((224, 224))

    a_image = a_tensor.argmax(axis=2)
    b_image = b_tensor.argmax(axis=2)

    a_image = a_image * 184.439 / NR_BINS - 86.185
    b_image = b_image * 202.345 / NR_BINS - 107.863


    ab_dec = np.array([a_image, b_image]).transpose(1, 2, 0)


    ab_dec_us = sni.zoom(ab_dec, (1. * H_orig / H_out, 1. * W_orig / W_out, 1))
    img_lab_out = np.concatenate((img_l[:, :, np.newaxis], ab_dec_us), axis=2)
    img_rgb_out = (255 * np.clip(color.lab2rgb(img_lab_out), 0, 1)).astype('uint8')

    return img_rgb_out


def DanielNet(include_top=True,
              weights='imagenet',
              input_tensor=None,
              input_shape=None,
              pooling=None,
              classes=1000):
    """Instantiates the DanielNet architecture

    # Arguments:
        include_top: whether to include the 3 fully-connected
            layers at the top of the network
        weights: one of 'None' (random initialization)
            or "imagenet" (pre-training on Imagenet)
        input_tensor: optional Keras tensor (i.e output of 'layers.Input()')
            to use as image input for the model.
        input_shape: optional shape tuple, only to be specified if 'include_top'
            is False (otherwise the input shape has to be (224, 224, 3) with the channels
            last data format or (3, 224, 224) with channels_first data format). It
            should have exactly 3 input channels, and width and height should be no smaller
            than 48.
        pooling: Optional pooling mode for feature extraction when
            'include_top' is False.
            - None means that the output of the modell will be the 4D
              tensor output of the last convolutional layer.
            - avg means that global average pooling will be applied to the output of
              the last convolutional layer, and thus the output of the model will be
              a 2D tensor.
            - max means that global max pooling will be applied
        classes: optional number of classes to classify images into, only to be
            specified if 'include_top' is True, and if no 'weights' argument is specified.

    # Return:
        A Keras model instance.

    # Raises:
        ValueError: in case of invalid argument for 'weights', or invaldi input shape
    """

    if weights not in ['imagenet', None]:
        raise ValueError('The weights argument should be either ',
                         'None(random initialization), or ',
                         '`imagenet`(pre-training on ImageNet).')

    if weights == 'imagenet' and include_top and classes != 1000:
        raise ValueError('If using `weights` as imagenet with ',
                         '`include_top` as true ,`classes` should be 1000.')

    # Determine proper input shape
    input_shape = _obtain_input_shape(input_shape=input_shape,
                                      default_size=224,
                                      min_size=48,
                                      data_format=K.image_data_format(),
                                      include_top=include_top)

    if input_tensor is None:
        img_input = Input(shape=input_shape)
    else:
        if not K.is_keras_tensor(input_tensor):
            img_input = Input(tensor=input_tensor, shape=input_shape)
        else:
            img_input = input_tensor


    if K.image_data_format() == 'channels_last':
        bn_axis = 3
    else:
        bn_axis = 1

    # Instantiate VGG16

    # Block 1
    head = Conv2D(filters=64,
                  kernel_size=(3, 3),
                  strides=(1, 1),
                  activation='relu',
                  padding='same',
                  name='block1_conv1')(img_input)
    head = Conv2D(filters=64,
                  kernel_size=(3, 3),
                  strides=(1, 1),
                  padding='same',
                  name='block1_conv2')(head)
    head = Activation('relu')(head)
    head = MaxPooling2D(pool_size=(2, 2), strides=(2, 2), name='block1_pool')(head)

    # Block 2
    head = Conv2D(filters=128,
                  kernel_size=(3, 3),
                  strides=(1, 1),
                  activation='relu',
                  padding='same',
                  name='block2_conv1')(head)
    head = Conv2D(filters=128,
                  kernel_size=(3, 3),
                  strides=(1, 1),
                  activation='relu',
                  padding='same',
                  name='block2_conv2')(head)
    head = MaxPooling2D(pool_size=(2, 2), strides=(2, 2), name='block2_pool')(head)

    # Block 3
    head = Conv2D(filters=256,
                  kernel_size=(3, 3),
                  strides=(1, 1),
                  activation='relu',
                  padding='same',
                  name='block3_conv1')(head)
    head = Conv2D(filters=256,
                  kernel_size=(3, 3),
                  strides=(1, 1),
                  activation='relu',
                  padding='same',
                  name='block3_conv2')(head)
    head = Conv2D(filters=256,
                  kernel_size=(3, 3),
                  strides=(1, 1),
                  activation='relu',
                  padding='same',
                  name='block3_conv3')(head)
    head = MaxPooling2D(pool_size=(2, 2), strides=(2, 2), name='block3_pool')(head)

    # Block 4
    head = Conv2D(filters=512,
                  kernel_size=(3, 3),
                  strides=(1, 1),
                  activation='relu',
                  padding='same',
                  name='block4_conv1')(head)
    head = Conv2D(filters=512,
                  kernel_size=(3, 3),
                  strides=(1, 1),
                  activation='relu',
                  padding='same',
                  name='block4_conv2')(head)
    head = Conv2D(filters=512,
                  kernel_size=(3, 3),
                  strides=(1, 1),
                  activation='relu',
                  padding='same',
                  name='block4_conv3')(head)
    head = MaxPooling2D(pool_size=(2, 2), strides=(2, 2), name='block4_pool')(head)

    # Block 5
    head = Conv2D(filters=512,
                  kernel_size=(3, 3),
                  strides=(1, 1),
                  activation='relu',
                  padding='same',
                  name='block5_conv1')(head)
    head = Conv2D(filters=512,
                  kernel_size=(3, 3),
                  strides=(1, 1),
                  activation='relu',
                  padding='same',
                  name='block5_conv2')(head)
    head = Conv2D(filters=512,
                  kernel_size=(3, 3),
                  strides=(1, 1),
                  activation='relu',
                  padding='same',
                  name='block5_conv3')(head)
    head = MaxPooling2D(pool_size=(2, 2), strides=(2, 2), name='block5_pool')(head)


    if include_top:
        head = Flatten(name='flatten')(head)
        head = Dense(4096, activation='relu', name='fc1')(head)
        head = Dense(4096, activation='relu', name='fc2')(head)
        head = Dense(1000, activation='softmax', name='fc1000')(head)
    else:
        if pooling == 'avg':
            head = GlobalAveragePooling2D()(head)
        elif pooling == 'max':
            head = GlobalMaxPooling2D()(head)

    # Ensure that the model takes into account
    # any potential predecessors of `input_tensor`
    if input_tensor is not None:
        inputs = get_source_inputs(input_tensor)
    else:
        inputs = img_input

    vgg = Model(inputs=inputs, outputs=head, name='VGG16')

    # Load weights
    if weights == 'imagenet':
        if include_top:
            weights_path = get_file(fname='vgg16_weights_tf_dim_ordering_tf_kernels.h5',
                                    origin=WEIGHTS_PATH,
                                    cache_subdir='models')
            nr_cut_layers = 9# 13
        else:
            weights_path = get_file(fname='vgg16_weights_tf_dim_ordering_tf_kernels_notop.h5',
                                    origin=WEIGHTS_PATH_NO_TOP,
                                    cache_subdir='models')
            nr_cut_layers = 5# 9

        vgg.load_weights(weights_path)

        # Cutting layers with high level features
        for i in range(nr_cut_layers):
            print('Popping {}'.format(vgg.layers[-1].name))
            vgg.layers.pop()

        # Freeze the VGG16 layers
        for layer in vgg.layers:
            layer.trainable = False

        # All the layers are FREEZED up until this point
        # ---------------------------------------------------------------------

        model = UpSampling2D((2, 2))(vgg.layers[-1].output)
        b3c3_bn = vgg.get_layer(name='block3_conv3').output

        b3c3_bn = BatchNormalization(axis=bn_axis, name='upper_features_bn1')(b3c3_bn)
        b3c3_bn = Activation('relu')(b3c3_bn)

        model = layers.concatenate([model, b3c3_bn])

        # First concat
        model = UpSampling2D((2, 2))(model)
        b2c2_bn = vgg.get_layer(name='block2_conv2').output

        b2c2_bn = BatchNormalization(axis=bn_axis, name='block2_conv2_bn')(b2c2_bn)
        b2c2_bn = Activation('relu')(b2c2_bn)
        model = layers.concatenate([model, b2c2_bn])

        # Second concat
        model = UpSampling2D((2, 2))(model)
        model = Conv2D(filters=64,
                       kernel_size=(3, 3),
                       strides=(1, 1),
                       padding='same',
                       activation='relu',
                       name='conv_upsampled_2d')(model)

        b1c2_bn = vgg.get_layer(name='block1_conv2').output
        b1c2_bn = BatchNormalization(axis=bn_axis, name='block1_conv2_bn', trainable=True)(b1c2_bn)
        b1c2_bn = Activation('relu')(b1c2_bn)
        model = layers.concatenate([model, b1c2_bn])

        # Third concat
        model = layers.concatenate([model, img_input]) # VGG16 semantic information

        model = Conv2D(filters=128,
                       kernel_size=(3, 3),
                       strides=(1, 1),
                       padding='same',
                       name='block_residual_conv')(model)

        model = Dropout(rate=0.5)(model)
        model = Activation('relu', name='activation_residual')(model)

        model = Conv2D(filters=128,
                       kernel_size=(3, 3),
                       strides=(1, 1),
                       padding='same',
                       name='block_new_conv1')(model)

        model = BatchNormalization(axis=bn_axis, name='block_new_bn1')(model)
        model = Activation('relu')(model)

        output_b_channel = Conv2D(filters=128,
                                  kernel_size=(3, 3),
                                  strides=(1, 1),
                                  padding='same',
                                  name='block_new_conv2')(model)

        output_b_channel = BatchNormalization(axis=bn_axis, name='block_new_bn2')(output_b_channel)
        output_b_channel = Activation('relu')(output_b_channel)
        model = Conv2D(filters=128,
                       kernel_size=(3, 3),
                       strides=(1, 1),
                       padding='same',
                       name='block_new_conv_bn2')(model)
        model = BatchNormalization(axis=bn_axis, name='block_new_bn3')(model)

        model = Activation('relu')(model)

        output_b_channel = Conv2D(filters=NR_BINS,
                                  kernel_size=(1, 1),
                                  strides=(1, 1),
                                  activation='relu',
                                  padding='valid',
                                  name='block_new_conv5')(output_b_channel)

        output_b_channel = BatchNormalization(axis=bn_axis, name='block_new_bn5')(output_b_channel)
        output_b_channel = Activation('relu')(output_b_channel)
        output_b_channel = Conv2D(filters=NR_BINS,
                                  kernel_size=(1, 1),
                                  strides=(1, 1),
                                  padding='valid',
                                  name='block_new_conv_b')(output_b_channel)

        model = Conv2D(filters=NR_BINS,
                       kernel_size=(1, 1),
                       strides=(1, 1),
                       padding='valid',
                       name='block_new_conv4')(model)
        model = BatchNormalization(axis=bn_axis, name='block_new_bn4')(model)
        model = Activation('relu')(model)

        model = Conv2D(filters=NR_BINS,
                       kernel_size=(1, 1),
                       strides=(1, 1),
                       padding='valid',
                       name='block_new_conv_a')(model)

        output_b_channel = Reshape(target_shape=(224 * 224, NR_BINS))(output_b_channel)
        model = Reshape(target_shape=(224*224, NR_BINS))(model)
        model = Activation('softmax', name='output_a_channel')(model)
        output_b_channel = Activation('softmax', name='output_b_channel')(output_b_channel)

        model = Model(inputs=inputs, outputs=[model, output_b_channel], name='DanielNet')

        if K.backend() == 'theano':
            layer_utils.convert_all_kernels_in_model(model)

        if K.image_data_format() == 'channels_first':
            if include_top:
                maxpool = model.get_layer(name='block5_pool')
                shape = maxpool.output_shape[1:]
                dense = model.get_layer('fc1')
                layer_utils.convert_dense_weights_data_format(dense=dense,
                                                              previous_feature_map_shape=shape,
                                                              target_data_format='channels_first')
            if K.backend() == 'tensorflow':
                warnings.warn('You are using the TensorFlow backend, ',
                              'yet you are using the Theano image data ',
                              'format convention (`image_data_format="channels_first".) ',
                              'For best performance, set '
                              '`image_data_format="channels_last"` in ',
                              'your Keras config at ~/.keras/keras.json')

    return model


def imagenet_generator(number, validation=False):
    batch_features = []
    batch_labels_a_channel = []
    batch_labels_b_channel = []

    if validation:
        image_targets = _get_files(TRAIN_PATH)[-number:]
    else:
        image_targets = _get_files(TRAIN_PATH)[:number] # change to negative for full

    while True:
        for image_name in image_targets:
            if '.floyddata' in image_name:
                continue
            l_channel, a_channel, b_channel = prepare_image(image_name, target_size=(224, 224))
            batch_features.append(l_channel)
            batch_labels_a_channel.append(a_channel)
            batch_labels_b_channel.append(b_channel)
            if len(batch_features) % BATCH_SIZE == 0:
                yield [np.array(batch_features)], [np.array(batch_labels_a_channel), np.array(batch_labels_b_channel)]
                del batch_features[:]
                del batch_labels_a_channel[:]
                del batch_labels_b_channel[:]


if __name__ == '__main__':
    print(' :: Loading DanielNet')
    model = DanielNet(include_top=False, weights='imagenet', input_shape=(224, 224, 3))
    print('Model loaded successfully')

#    plot_model(model, to_file='daniel_net_floyd_classification.png', show_layer_names=True, show_shapes=True)

    model.compile(loss={'output_a_channel': 'categorical_crossentropy', 'output_b_channel': 'categorical_crossentropy'},
                  loss_weights={'output_a_channel': 1.0, 'output_b_channel': 1.0},
                  optimizer='nadam',
                  metrics=['accuracy'])

    nr_images = len(list_files(TRAIN_PATH))

    print('Nr. of images: {}'.format(nr_images))
    print(' :: Starting training: {} iterations per {} epochs'.format(nr_images//BATCH_SIZE, EPOCHS))
    print(' :: Training images  :', nr_images * 0.9 // BATCH_SIZE * BATCH_SIZE)
    print(' :: Validation images:', nr_images * 0.1 // BATCH_SIZE * BATCH_SIZE)

    model.fit_generator(
        generator=imagenet_generator(int(nr_images * 0.9)),
        steps_per_epoch=nr_images * 0.9 // BATCH_SIZE,
        epochs=EPOCHS,
        callbacks=[ModelCheckpoint('/output/model_floyd.h5', save_best_only=True)],
        verbose=1,
        validation_data=imagenet_generator(int(nr_images * 0.1), validation=True),
        validation_steps=nr_images * 0.1 // BATCH_SIZE,
    )

    print(' :: Done. Saving weights.')
    model.save_weights('/output/first_try_floyd.h5')
